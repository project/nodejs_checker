<h2>Overview</h2>
==============
Nodejs checker provides a simple block that monitor Nodejs server status and
user's connection to the Nodejs server realtime. It also provide a tinny yet
useful API so that modules could handle Nodejs server state changes.

<h2>Features</h2>
==============
- Monitor Nodejs server and user connection to Nodejs from a block.
- Offer an API to handle Nodejs state change.

<h2>Requirements</h2>
===============
- <a href="http://nodejs.org/">Nodejs server</a>.
- <a href="/project/nodejs">Nodejs integration</a> module.

For installation guide : See INSTALL.txt

<h2>Known problems</h2>
==================
=> No known issue at the moment. 

<h2>Credits</h2>
==============
Module developed by Julien Garcia for the French Free Flying Federation
(Fédération Française de Vol Libre) and the french ministry of sports.
"Please contribute to Drupal anyway you can".
